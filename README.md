# XMonad Configuration (with additional packages)

## Info
This repo contains configuration files for:
* [xmonad](https://github.com/xmonad/xmonad)
* [xmobar](https://github.com/jaor/xmobar)
* [stalonetray](http://stalonetray.sourceforge.net/)
* [picom](https://github.com/yshui/picom)

as well as submodules from the xmonad and xmobar repositories. This is because it is installed using [stack](https://docs.haskellstack.org/en/stable/README/) and we need the git repositories.

[This](https://brianbuccola.com/how-to-install-xmonad-and-xmobar-via-stack/) tutorial explains how it's done (with some minor tweaks)

## Installation


1) Clone the repo with `git clone --recurse-submodules https://gitlab.com/dimitrissp/xmonad-configuration.git ~/.xmonad`
2) Go into the directory with `cd ~/.xmonad` and run `make` to install everything
