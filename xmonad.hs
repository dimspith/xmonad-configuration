{-# OPTIONS_GHC -W -fno-warn-missing-signatures #-}

import System.Exit (exitSuccess)
import System.IO
import XMonad
import XMonad.Config.Desktop
import qualified XMonad.StackSet as W

import XMonad.Util.EZConfig
import XMonad.Util.Run
import XMonad.Util.SpawnOnce
import XMonad.Util.WorkspaceCompare

import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.SetWMName

import XMonad.Prompt
import XMonad.Prompt.FuzzyMatch
import XMonad.Prompt.RunOrRaise
import XMonad.Prompt.XMonad

import XMonad.Actions.CopyWindow (kill1)
import XMonad.Actions.RotSlaves
import XMonad.Actions.SpawnOn
import XMonad.Actions.WithAll
import XMonad.Actions.GridSelect

import XMonad.Layout.Circle
import XMonad.Layout.Fullscreen
import XMonad.Layout.ResizableTile
import XMonad.Layout.TwoPanePersistent

import XMonad.Layout.NoBorders
import XMonad.Layout.ToggleLayouts as TL

-----------------------------
--- Colors (Gotham theme) ---
-----------------------------
myBackgroundColor = "#000000"

myContentColor = "#ffffff"

myRedColor = "#cf6a4c"

myGreenColor = "#99ad6a"

myYellowColor = "#fad07a"

myMagentaColor = "#8197bf"

myCyanColor = "#8fbfdc"

------------------------------
--- Variables and Settings ---
------------------------------
myModMask = mod4Mask

myBorderWidth = 2

myFont = "xft:Caskaydia Cove Nerd Font:regular:pixelsize=14"

myTerminal = "alacritty"

mySecondaryTerminal = "termite"

myEditor = "emacsclient -c"

myBrowser = "firefox"

myFileManager = myTerminal ++ " -e vifm"

myProgramLauncher = "rofi -show drun"

myOOMKillCommand = "kill -9 $(ps aux --sort=-%mem | awk 'NR==2{print $0}' | awk '{print $2}')"
---------------
--- Configs ---
---------------

-- Config for XMonad prompts.
myXPromptConfig :: XPConfig
myXPromptConfig =
  XPC
    { font = myFont
    , bgColor = myBackgroundColor
    , fgColor = myContentColor
    , bgHLight = myBackgroundColor
    , fgHLight = myContentColor
    , borderColor = myBackgroundColor
    , promptBorderWidth = 0
    , alwaysHighlight = True
    , height = 22
    , historySize = 0
    , position = Top
    , autoComplete = Nothing
    , showCompletionOnTab = False
    , searchPredicate = fuzzyMatch
    , defaultPrompter = id
    , sorter = const id
    , maxComplRows = Just 5
    , promptKeymap = defaultXPKeymap
    , completionKey = (0, xK_Tab)
    , changeModeKey = xK_grave
    , historyFilter = id
    , defaultText = []
    }

-- Pretty printer for xmonad workspaces.
myXmobarPP :: PP
myXmobarPP =
  PP
    { ppCurrent = xmobarColor myYellowColor "" . wrap "[" "]"
    , ppVisible = xmobarColor myCyanColor   "" . wrap "<" ">"
    , ppHidden = id
    , ppHiddenNoWindows = const ""
    , ppVisibleNoWindows = Nothing
    , ppUrgent = xmobarColor myRedColor ""
    , ppSep = " : "
    , ppWsSep = " "
    , ppTitle = xmobarColor myGreenColor "" . shorten 40
    , ppTitleSanitize = xmobarStrip . dzenEscape
    , ppLayout = xmobarColor myMagentaColor ""
    , ppOrder = id
    , ppOutput = putStrLn
    , ppSort = getSortByIndex
    , ppExtras = []
    }

-- Colorizer
myColorizer :: Window -> Bool -> X (String, String)
myColorizer = colorRangeFromClassName
  (0xfa,0xd0,0x7a) -- Lowest inactive bg
  (0x99,0xad,0x6a) -- Highest inactive bg
  (0x00,0x00,0x00) -- Active bg
  (0x00,0x00,0x00) -- Inactive fg
  (0xff,0xff,0xff) -- Active fg


-- Grid select config
myGSConfig :: p -> GSConfig Window
myGSConfig colorizer =
  (buildDefaultGSConfig myColorizer)
  { gs_cellheight = 30
  , gs_cellwidth = 200
  , gs_cellpadding = 8
  , gs_originFractX = 0.5
  , gs_originFractY = 0.5
  , gs_rearranger = noRearranger
  , gs_font = myFont
  }

-- Spawn selected windows from a name-command list
spawnSelected' :: [(String, String)] -> X ()
spawnSelected' lst = gridselect conf lst >>= flip whenJust spawn
    where conf = def

----------------------------------------------------------------------
-- MAIN
----------------------------------------------------------------------
main :: IO ()
main = do
  xmobar1 <- spawnPipe "xmobar ~/.xmonad/xmobar-config/xmobarrc"
  _ <- spawnPipe "pkill stalonetray;stalonetray -c ~/.xmonad/stalonetray-config/stalonetrayrc"
  xmonad $
    fullscreenSupport $
    desktopConfig
      { terminal = myTerminal
      , modMask = myModMask
      , borderWidth = myBorderWidth
      , normalBorderColor = myBackgroundColor
      , focusedBorderColor = myYellowColor
      , manageHook = myManageHook
      , layoutHook = myLayoutHook
      , handleEventHook = myHandleEventHook
      , logHook = myLogHook xmobar1
      , startupHook = myStartupHook
      , workspaces = myWorkspaces
      } `additionalKeysP`
    myKeyBindings `removeKeysP`
    removedKeybindings

----------------------------------------------------------------------
-- LOOK AND FEEL
----------------------------------------------------------------------
myWorkspaces :: [String]
myWorkspaces =
  [ "1"
  , "2"
  , "3"
  , "4"
  , "5"
  , "6"
  , "7"
  , "8"
  ]

----------------------------------------------------------------------
-- XMONAD STATUS BAR CONFIG
----------------------------------------------------------------------
myLogHook :: Handle -> X ()
myLogHook xmobar1 = dynamicLogWithPP $ myXmobarPP {ppOutput = hPutStrLn xmobar1}

----------------------------------------------------------------------
-- LAYOUT CONFIG
----------------------------------------------------------------------


-- myLayoutHook =
--   ((avoidStruts . fullscreenFull . toggleLayouts fullscreen . smartBorders) $
--    tall ||| Circle ||| TwoPanePersistent Nothing (3 / 100) (2 / 3)) |||
--   fullscreen
--   where
--     tall = ResizableTall 1 (3 / 100) (1 / 2) []
--     fullscreen = noBorders $ fullscreenFocus Full

myLayoutHook =
      defMods (tall ||| Circle ||| readLayout) ||| fullscreen
  where
    tall       = ResizableTall 1 (3 / 100) (1 / 2) []
    fullscreen = noBorders $ fullscreenFocus Full
    readLayout = TwoPanePersistent Nothing (3 / 100) (2 / 3)
    defMods    = avoidStruts
               . fullscreenFull
               . toggleLayouts fullscreen
               . smartBorders




----------------------------------------------------------------------
-- MANAGE HOOK CONFIG
----------------------------------------------------------------------
myManageHook :: ManageHook
myManageHook = composeAll
  [ manageDocks
  , manageHook def
  , manageSpawn

  -- Certain windows float to the center of the screen
  , isDialog --> doCenterFloat
  , className =? "Nm-connection-editor" --> doCenterFloat
  , className =? "Nitrogen" --> doCenterFloat

  -- Send certain windows to certain workspaces
  , className =? "Lollypop" --> doShift "5"
  , className =? "Steam" --> doShift "6"
  , className =? "TelegramDesktop" --> doShift "7"
  , className =? "discord" --> doShift "7"
  ]

----------------------------------------------------------------------
-- KEYBINDINGS CONFIG
----------------------------------------------------------------------
myKeyBindings :: [(String, X ())]
myKeyBindings =
  [ ("M-C-q",       io exitSuccess)
  , ("M-S-q",       kill1)
  , ("M-f",         sendMessage ToggleStruts >> sendMessage ToggleLayout)
  , ("M-S-<Space>", sendMessage ToggleStruts)
  , ("M-x",         xmonadPrompt myXPromptConfig)
  , ("M-C-a",       killAll)
  , ("M-`",         spawnHere "xkill")
  , ("M-S-C-q",     spawnHere myOOMKillCommand)


  -- Window manipulation and navigation
  , ("M-j", windows W.focusDown)
  , ("M-k", windows W.focusUp)

  , ("M-S-j", windows W.swapDown)
  , ("M-S-k", windows W.swapUp)

  , ("M-m", windows W.focusMaster)
  , ("M-S-m", windows W.swapMaster)

  , ("M-S-h", rotAllUp)
  , ("M-S-l", rotAllDown)

  , ("M-,", sendMessage (IncMasterN 1))
  , ("M-.", sendMessage (IncMasterN (-1)))


  -- Applications
  , ("M-<Return>",   spawnHere myTerminal)
  , ("M-S-<Return>", spawnHere mySecondaryTerminal)
  , ("M-S-e",        spawnHere myEditor)
  , ("M-S-b",        spawnHere myBrowser)
  , ("M-S-t",        spawnHere (myEditor ++ " ~/notes/schedule.org"))
  , ("M-C-m",        spawnHere "pavucontrol")
  , ("M-S-f",        spawnHere myFileManager)
  , ("M-S-p",        spawnHere myProgramLauncher)
  , ("M-p",          runOrRaisePrompt myXPromptConfig)
  , ("M-i",          goToSelected $ myGSConfig myColorizer)
  , ("M-S-i",        spawnSelected'
      [ ("Firefox", "firefox"),
        ("Emacs", "emacsclient -c || emacs"),
        ("Lutris", "lutris"),
        ("Steam", "steam"),
        ("Telegram", "telegram-desktop"),
        ("Discord", "discord"),
        ("Inkscape", "inkscape")
      ])
  ]
removedKeybindings :: [String]
removedKeybindings = ["M-q"]

----------------------------------------------------------------------
-- STARTUP CONFIG
----------------------------------------------------------------------
myStartupHook :: X ()
myStartupHook = do
  spawnOnce "/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1"
  spawnOnce "nitrogen --restore"
  spawnOnce "nm-applet"
  spawnOnce "volumeicon"
  spawnOnce "emacs --daemon"
  -- spawnOnce "picom --config ~/.xmonad/picom-config/picom.conf -b"
  spawnOnce "setxkbmap -model pc105 -layout us,gr -option grp:rctrl_toggle -option ctrl:nocaps"
  spawnOnce "xfce4-power-manager"
  spawnOnce "emacs --daemon"
  setWMName "LG3D"

----------------------------------------------------------------------
-- HANDLE EVENT HOOK CONFIG
----------------------------------------------------------------------
myHandleEventHook = composeAll [docksEventHook]
