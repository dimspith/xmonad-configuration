install: install-stack install-deps install-misc build-proj build copy-desktop-file

install-stack:
	@curl -sSL https://get.haskellstack.org/ | sh 2>/dev/null ||\
		echo -e "\e[0;32mStack is installed!\e[0m"

build:
	@./build xmonad-x86_64-linux

build-proj:
	@stack setup
	@stack install

install-deps:
	@sudo pacman -Sq --needed stalonetray picom network-manager-applet volumeicon

install-misc:
	@sudo pacman -Sq --needed alacritty kitty emacs firefox nnn rofi awk\
		pavucontrol nitrogen xorg-setxkbmap polkit-gnome

copy-desktop-file:
	@sudo cp -r xmonad.desktop /usr/share/xsessions/
	@echo -e "\e[0;32mCopied desktop file to /usr/share/xsessions\e[0m"

uninstall:
	@sudo rm -rf /usr/share/xsessions/xmonad.desktop
	@sudo rm -rf ~/.local/bin/{xmonad,xmonad-x86_64-linux,xmobar}
